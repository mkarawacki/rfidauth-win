
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package rfidauth;

//~--- non-JDK imports --------------------------------------------------------

import gnu.io.CommPortIdentifier;

//~--- JDK imports ------------------------------------------------------------

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import com.fazecast.jSerialComm.*;
/**
 *
 * @author Ja
 */
public class RFIDauth {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // TODO code application logic here
        Enumeration ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements()) {
            CommPortIdentifier port = (CommPortIdentifier) ports.nextElement();
            String             type;

            switch (port.getPortType()) {
            case CommPortIdentifier.PORT_PARALLEL :
                type = "Parallel";

                break;

            case CommPortIdentifier.PORT_SERIAL :
                type = "Serial";

                break;

            default :    // / Shouldn't happen
                type = "Unknown";

                break;
            }

            FriendlyName fn = new FriendlyName();

            System.out.println(port.getName() + ": " + type + " " + fn.get(port.getName()));
        }
    }

    public static Map<String, String> getCOMportDict() {
        Map<String, String> COMdict = new HashMap<String, String>();

        COMdict.clear();

        Enumeration ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements()) {
            CommPortIdentifier port = (CommPortIdentifier) ports.nextElement();
            String             type;

            switch (port.getPortType()) {
            case CommPortIdentifier.PORT_PARALLEL :
                type = "Parallel";

                break;

            case CommPortIdentifier.PORT_SERIAL :
                type = "Serial";

                break;

            default :    // / Shouldn't happen
                type = "Unknown";

                break;
            }

            FriendlyName fn = new FriendlyName();

            COMdict.put(port.getName(), fn.get(port.getName()));
        }

        return COMdict;
    }

    public static List<String> getCOMportList() {
        List<String> COMlist = new Vector<String>();

        COMlist.clear();

        Enumeration ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements()) {
            CommPortIdentifier port = (CommPortIdentifier) ports.nextElement();
            String             type;

            switch (port.getPortType()) {
            case CommPortIdentifier.PORT_PARALLEL :
                type = "Parallel";

                break;

            case CommPortIdentifier.PORT_SERIAL :
                type = "Serial";

                break;

            default :    // / Shouldn't happen
                type = "Unknown";

                break;
            }

            // FriendlyName fn = new FriendlyName();
            COMlist.add(port.getName());
        }

        return COMlist;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
